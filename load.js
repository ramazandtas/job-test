(function(){
    var preload = document.getElementById("load");//Her tager den load og laver den om til preload
    var loading = 0;//loading skal starte fra 0
    var id = setInterval(frame, 64);

    function frame(){
        if(loading == 100){//Når loading kommer op på 100 så skal der ske det her
            clearInterval(id);//Slet nuværende side
            window.open("web.html", "_self");//Åbne den her side
        } else {
            loading = loading +1;
            if(loading == 90){
                preload.style.animation = "fadout 1s ease";
            }
        }
    }
})();